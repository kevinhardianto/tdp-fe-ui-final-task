import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="website">

      {/* Navbar (sit on top) */}
      <div>
        <div className='navbar'>
          <div className='navbarleft'>
            <a href="/#"><b>EDTS</b> TDP Batch #2</a>
          </div>
          <div className='navbarright'>
            <a id='project' href="#projects">Projects</a>
            <a id='about' href="#abouts">About</a>
            <a id='contact' href="#contacts">Contact</a>
          </div>
        </div>
      </div>

      {/* Header */}
      <div className='header'>
      <header>
        <img className='img1' src={banner} alt="Architecture" width={1500} height={800} />
        <div className='edts-container'>
          <h1 id='edts'>
              <span className='span1'>EDTS</span><span className='span2'>Architects</span>
          </h1>
        </div>
      </header>
      </div>

      {/* Page content */}
      <div className='container'>

        {/* Project Section */}
        <div id="projects">
          <h3>Projects</h3>
          <hr></hr>
          <br></br>
        </div>
        
        <div className='project-container1'>
          <div className='image'>
            <div className='desc'>Summer House</div>
            <img className='img' src={house2} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Brick House</div>
            <img className='img' src={house3} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Renovated</div>
            <img className='img' src={house4} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Barn House</div>
            <img className='img' src={house5} alt="House" />
          </div>
        </div>
        <div className='project-container2'>
          <div className='image'>
            <div className='desc'>Summer House</div>
            <img className='img' src={house3} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Brick House</div>
            <img className='img' src={house2} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Renovated</div>
            <img className='img' src={house5} alt="House" />
          </div>
          <div className='image'>
            <div className='desc'>Barn House</div>
            <img className='img' src={house4} alt="House" />
          </div>
        </div>

        {/* About Section */}
        <div id='abouts'>
          <h3>About</h3>
          <hr></hr>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
          <br></br>
        </div>
        <div className='about-container'>
          <div className='about'>
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p>CEO &amp; Founder</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about'>
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about'>
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div className='about'>
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}
        <div id='contacts'>
          <h3>Contact</h3>
          <hr></hr>
          <p>Lets get in touch and talk about your next project.</p>
          <form>          
            <div>
              <label for="name"></label>
              <input type="text" id="name" name="name" placeholder='Name' className='input'/>
            </div>
            <div>
              <label for="email"></label>
              <input type="email" id="email" name="email" placeholder='Email' className='input'/>
            </div>
            <div>
              <label for="subject"></label>
              <input type="text" id="subject" name="subject" placeholder='Subject' className='input'/>
            </div>
            <div>
              <label for="comment"></label>
              <input type="text" id="comment" name="comment" placeholder='Comment' className='input'/>
            </div>
            <br></br>
            <input type="submit" value="SEND MESSAGE" class="send"></input>
          </form>
          <br></br>
        </div>

        {/* Image of location/map */}
        <div className='map'>
          <img src={map} alt="maps" />
        </div>

        <div className='contactlist'>
          <h3>Contact List</h3>
          <hr></hr>

          <table>
          <tr className='contact-title'>
            <th className='borderleft'>Name</th>
            <th>Email</th>
            <th>Country</th>
          </tr>
          <tr>
            <td className='borderleft'>Alfreds Futterkiste</td>
            <td>alfreds@gmail.com</td>
            <td>Germany</td>
          </tr>
          <tr>
            <td className='borderleft'>Reza</td>
            <td>reza@gmail.com</td>
            <td>Indonesia</td>
          </tr>
          <tr>
            <td className='borderleft'>Ismail</td>
            <td>ismail@gmail.com</td>
            <td>Turkey</td>
          </tr>
          <tr>
            <td className='borderleft'>Holand</td>
            <td>holand@gmail.com</td>
            <td>Belanda</td>
          </tr>
          </table>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
